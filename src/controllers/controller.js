
const factory = require('src/factory/factory'),
    Validation = require('src/validations/validations'),
    Joi = require('joi');


const response_format = Joi.object({
    is_success: Joi.boolean(),
    result: Joi.any(),
    message: Joi.string().required(),
    status_code: Joi.number().required(), 
    }),
    response = {
        status : {
            200: response_format,
            201: response_format,
            400: response_format,
            500: response_format,
        },
    };


exports.addNote = {
    description : "Add TODO Note",
    validate: Validation.addNote,
    auth: 'jwt',
    handler: (request, h) => {
        return factory.addNote(request, h);
    },
    response,
};

exports.updateNote = {
    description : "Update TODO Note",
    validate: Validation.updateNote,
    auth: 'jwt',
    handler: (request, h) => {
        return factory.updateNote(request, h);
    },
    response,
};

exports.getNotes = {
    description : "Get Notes per condition",
    validate: Validation.getNotes,
    auth: 'jwt',
    handler: (request, h) => {
        return factory.getNotes(request, h);
    },
    response,
};

exports.getNotesDetails = {
    description : "Get Notes Details per condition",
    validate: Validation.getNotesDetails,
    auth: 'jwt',
    handler: (request, h) => {
        return factory.getNotesDetails(request, h);
    },
    response,
};

exports.deleteNote = {
    description : "Delete Note",
    validate: Validation.deleteNote,
    auth: 'jwt',
    handler: (request, h) => {
        return factory.deleteNote(request, h);
    },
    response,
};

exports.getCSVReport = {
    description : "Get CSV Report",
    validate: Validation.getCSVReport,
    auth: 'jwt',
    handler: (request, h) => {
        return factory.getCSVReport(request, h);
    },
    response,
}
