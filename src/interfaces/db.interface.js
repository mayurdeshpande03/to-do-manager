
class dbInterface {
    constructor (_model) {
        this.model = _model;
    }

    createOne (modelDataObject) {
        return new Promise((resolve, reject) => {
            let modelObject = new this.model();

            Object.assign(modelObject, modelDataObject);

            modelObject.save(function(err, result) {
                if (err) {
                    
                    return reject(err);
                }

                resolve(result);
            });
        });
    }

    findOne (condition, projection) {
        return new Promise((resolve, reject) => {
            if (!projection) {
                projection = {};
            }

            this.model.findOne(condition, projection, (err, result) => {
                if (err) {
                    return reject(err);
                }

                resolve(result);
            });
        });
    }

    updateOne (condition, updateFields) {
        return new Promise((resolve, reject) => {
            this.model.updateOne(condition, updateFields, function(err, result) {
                if (err) {
                    return reject(err);
                }

                resolve(result);
            });
        });
    }

    findAll (condition, projection) {
        return new Promise((resolve, reject) => {
            this.model.find(condition, projection, (err, result) => {
                if (err) {
                    return reject(err);
                }
                resolve(result);
            });
        });
    }

    deleteOne (condition) {
        return new Promise((resolve, reject) => {
            this.model.deleteOne(condition, function(err, result) {
                if (err) {
                    return reject(err);
                }

                resolve(result);
            });
        });
    }
}

module.exports = dbInterface;
