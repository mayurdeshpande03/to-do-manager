let mongoose = require("mongoose"),
    Schema = mongoose.Schema;

let notesSchema = new Schema(
    {
        title: {
            type: Schema.Types.String,
            default: ''
        },
        description: {
            type: Schema.Types.String,
            default: ''
        },
        status: {
            type: Schema.Types.String,
            default: ''
        },
        user: {
            type: Schema.Types.String,
            default: ''
        },
        due_date: {
            type: Schema.Types.String,
            default: ''
        }
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        },
    },
    {strict: false }
);
module.exports = mongoose.model("notes", notesSchema, "notes");
