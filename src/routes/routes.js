/*
 * Notification Route File
 */

const controller = require('src/controllers/controller');

module.exports = [
	{
		method: 'POST',
		path: '/add-note',
		config: controller.addNote
	},
	{
		method: 'PUT',
		path: '/update-note/{id}',
		config: controller.updateNote
	},
	{
		method: 'GET',
		path: '/get-notes',
		config: controller.getNotes
	},
	{
		method: 'GET',
		path: '/get-note/{id}',
		config: controller.getNotesDetails
	},
	{
		method: 'DELETE',
		path: '/delete-note/{id}',
		config: controller.deleteNote
	},
	{
		method: 'GET',
		path: '/get-csv-report',
		config: controller.getCSVReport
	}
];
