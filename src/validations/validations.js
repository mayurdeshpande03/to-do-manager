/*
 * requests Validations File
 */

const { allow } = require('joi');
const Joi = require('joi');

module.exports = Object.freeze({
    addNote: {
        payload: {
            title: Joi.string().required().example('ABC'),
            description: Joi.string().optional().example('DEF'),
            status: Joi.string().required().example('abc.def@xyz.com'),
            user: Joi.string().required().example('12345'),
            due_date: Joi.string().required().example('2021-07-04'),
        },
        headers: Joi.object({'authorization': Joi.string().required()}).options({allowUnknown: true})
    },

    updateNote:{
        params: {
            id: Joi.string().required()
        },
        payload: {
            title: Joi.string().example('abc.def@xyz.com'),
            description: Joi.string().example('12345'),
            due_date: Joi.string().example('12345'),
            status: Joi.string().example('12345'),
        },
        headers: Joi.object({'authorization': Joi.string().required()}).options({allowUnknown: true})
    },

    getNotes: {
        headers: Joi.object({'authorization': Joi.string().required()}).options({allowUnknown: true})
    },

    getNotesDetails: {
        params: {
            id: Joi.string().required().example('1')
        },
        headers: Joi.object({'authorization': Joi.string().required()}).options({allowUnknown: true})    
    },

    deleteNote: {
        params: {
            id: Joi.string().required().example('1')
        },
        headers: Joi.object({'authorization': Joi.string().required()}).options({allowUnknown: true})        
    },

    getCSVReport: {
        headers: Joi.object({'authorization': Joi.string().required()}).options({allowUnknown: true})
    }
});
