const { required } = require("joi");

const Response = require("src/utils/response"),
    StatusCodes = require("src/utils/status_codes"),
    ResponseMessages = require("src/utils/response_messages"),
    to = require("src/utils/promise_handler"),
    model = require('src/models/model'),
    dbInterface = require("src/interfaces/db.interface"),
    _ = require('lodash'),
    moment = require('moment');
    Interface = new dbInterface(model),
    ObjectsToCsv = require('objects-to-csv');
var { Parser } = require('json2csv');



/**
 * @param request
 * @param h
 * @private
 */
exports.addNote = async (request, h) => {

    request.payload.due_date = moment(request.payload.due_date).format('YYYY-MM-DD');
    
    try {
        [err, response_data] = await to(Interface.createOne(request.payload));
        if (err) {
            return h.response(Response.sendResponse(false, err,
                ResponseMessages.ERROR,
                StatusCodes.BAD_REQUEST,),
            ).code(StatusCodes.BAD_REQUEST);
        } else {
            return h.response(Response.sendResponse(true, response_data,
                ResponseMessages.SUCCESS,
                StatusCodes.OK,),
            ).code(StatusCodes.OK);
        }
    } catch (err) {
        return h.response(Response.sendResponse(false, err,
            ResponseMessages.ERROR,
            StatusCodes.BAD_REQUEST,),
        ).code(StatusCodes.BAD_REQUEST);
    }
};


/**
 * @param request
 * @param h
 * @private
 */
exports.updateNote = async function (request, h) {
    let response_data = null;

    if (request.payload.due_date) {
        request.payload.due_date = moment(request.payload.due_date,).format('YYYY-MM-DD');
    }

    try {
        [err, response_data] = await to(Interface.updateOne({_id: request.params.id}, request.payload));
        if (err) {
            return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
        } else {
            
            return h.response(Response.sendResponse(true, null, ResponseMessages.SUCCESS, StatusCodes.OK)).code(StatusCodes.OK);
        }
    } catch (err) {
        return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
    }
};

/**
 * @param request
 * @param h
 * @private
 */
exports.getNotes = async function (request, h) {
    let response_data = null;
    
    try {
        [err, response_data] = await to(Interface.findAll({}, {}));
        if (err) {

            return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
        } else {

            return h.response(Response.sendResponse(true, response_data, ResponseMessages.SUCCESS, StatusCodes.OK)).code(StatusCodes.OK);
        }
    } catch (err) {
        return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
    }
};


/**
 * @param request
 * @param h
 * @private
 */
 exports.getNotesDetails = async function (request, h) {
    let response_data = null;

    try {
        [err, response_data] = await to(Interface.findOne({_id: request.params.id}, request.query));
        if (err) {
            return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
        } else {

            return h.response(Response.sendResponse(true, response_data, ResponseMessages.SUCCESS, StatusCodes.OK)).code(StatusCodes.OK);
        }
    } catch (err) {
        return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
    }
};

/**
 * @param request
 * @param h
 * @private
 */
exports.deleteNote = async function (request, h) {
    let response_data = null;

    try {

        [err, response_data] = await to(Interface.deleteOne({_id: request.params.id}));
        if (err) {
            return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
        } else {

            return h.response(Response.sendResponse(true, response_data, ResponseMessages.SUCCESS, StatusCodes.OK)).code(StatusCodes.OK);
        }
    } catch (err) {
        return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
    }
};


/**
 * @param request
 * @param h
 * @private
 */
 exports.getCSVReport = async function (request, h) {

    try {

        [err, response_data] = await to(Interface.findAll({}, {}));
        if (err) {

            return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
        } else {

            try {

                const csv = new ObjectsToCsv(JSON.parse(JSON.stringify(response_data)));
                await csv.toDisk('./report.csv')            
                return h.response(Response.sendResponse(true, null, ResponseMessages.SUCCESS, StatusCodes.OK)).code(StatusCodes.OK);
            } catch (error) {
    
                return h.response(Response.sendResponse(false, error, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
            }
        }
    } catch (err) {
        return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
    }
};

