FROM node:dubnium
  
WORKDIR /usr/src/to-do-manager

COPY package.json .

COPY . .

RUN npm install

CMD [ "npm", "start" ]
