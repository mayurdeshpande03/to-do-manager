/******************************* START IMPORT LIBRARIES *******************************/
process.env.NODE_ENV = "test";
const path = require("path");
require("app-module-path").addPath(path.join(__dirname, "../"));
const chai = require("chai"),
	chaiHttp = require("chai-http"),
	server = require("../server")
	should = chai.should(),
	qs = require("qs");
const mongo = require("../databases/mongodb");
chai.use(chaiHttp);
const assert = require('assert');
/******************************* END IMPORT LIBRARIES *******************************/

/******************************* START TEST FUNCTIONS *******************************/

describe("notes", () => {
	let result;
	let mongo_result, _id;
	before(async () => {
		// Before block
		try {
			global.sequelizeModels = null;
			result = await server.init();
			mongo_result = await mongo.init();
			console.log("Server Started :");
			return result;
		} catch (e) {
			console.log("Error in starting server : ", e);
			return e;
		}
	});

	describe("POST /add-note", () => {
		it("POST /add-note", done => {
			let options = {
				method: "POST",
				url: `/add-note`,
				payload: {
					title: "title",
					description: "description",
					status: "TODO",
					user: "Mayur Deshpande",
					due_date: "2021-07-04"
				},
				headers: {
					authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJwYXNzd29yZCJ9.LBRukwCS5z3saJkh1fQCBvCumu5QBuiLsAr6eWelxGw",
				},
			};
			chai
				.request("http://localhost:50000")
				.post(options.url)
				.set(options.headers)
				.send(options.payload)
				.end((err, res) => {
					
					_id = JSON.parse(res.text).result._id;
					console.log(err);
					res.should.have.status(200);
					done();
				});
		});
	});
	describe("PUT /update-note/{id}", () => {
		it("PUT /update-note/{id}", done => {
			let options = {
				method: "PUT",
                url: `/update-note/{id}`.replace(`{ id }`.replace(/ /g, ""), `${_id}`),
                payload:{
					title: "title",
					description: "description",
					status: "TODO",
					due_date: "2021-07-04"
                    },
				headers: {
					authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJwYXNzd29yZCJ9.LBRukwCS5z3saJkh1fQCBvCumu5QBuiLsAr6eWelxGw",
				},
			};
			chai
				.request("http://localhost:50000")
				.put(options.url)
				.set(options.headers)
				.send(options.payload)
				.end((err, res) => {
					res.should.have.status(200);
					done();
				});
		});
	});
	describe("GET /get-note/{id}", () => {
		it("GET /get-note/{id}", done => {
			let options = {
				method: "GET",
				url: `/get-note/{id}`.replace(`{ id }`.replace(/ /g, ""), `${_id}`),
				headers: {
					authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJwYXNzd29yZCJ9.LBRukwCS5z3saJkh1fQCBvCumu5QBuiLsAr6eWelxGw",
				},
			};
			chai
				.request("http://localhost:50000")
				.get(options.url)
				.set(options.headers)
				.end((err, res) => {
					res.should.have.status(200);
					done();
				});
		});
	});


	describe("GET /get-notes", () => {
		it("GET /get-notes", done => {
			let options = {
				method: "GET",
                url: `/get-notes`,
				headers: {
					authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJwYXNzd29yZCJ9.LBRukwCS5z3saJkh1fQCBvCumu5QBuiLsAr6eWelxGw",
				},
			};
			chai
				.request("http://localhost:50000")
				.get(options.url)
				.set(options.headers)
				.end((err, res) => {
					res.should.have.status(200);
					done();
				});
		});
	});

	describe("DELETE /delete-note/{id}", () => {
		it("DELETE /delete-note/{id}", done => {
			let options = {
				method: "DELETE",
				url: `/delete-note/{id}`.replace(`{ id }`.replace(/ /g, ""), `${_id}`),
				headers: {
					authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJwYXNzd29yZCJ9.LBRukwCS5z3saJkh1fQCBvCumu5QBuiLsAr6eWelxGw",
				},
			};
			chai
				.request("http://localhost:50000")
				.delete(options.url)
				.set(options.headers)
				.end((err, res) => {
					res.should.have.status(200);
					done();
				});
		});
	});

	describe("GET /get-csv-report", () => {
		it("GET /get-csv-report", done => {
			let options = {
				method: "GET",
                url: `/get-csv-report`,
				headers: {
					authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJwYXNzd29yZCJ9.LBRukwCS5z3saJkh1fQCBvCumu5QBuiLsAr6eWelxGw",
				},
			};
			chai
				.request("http://localhost:50000")
				.get(options.url)
				.set(options.headers)
				.end((err, res) => {
					res.should.have.status(200);
					done();
				});
		});
	});

	//..........................................Negative API Calls......................................//

	describe("POST /add-note", () => {
		it("POST /add-note", done => {
			let options = {
				method: "POST",
				url: `/add-note`,
				payload: {
					title: "title",
					description: "description",
					status: "TODO",
					// user: "Mayur Deshpande",
					due_date: "2021-07-04"
				},
				headers: {
					authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJwYXNzd29yZCJ9.LBRukwCS5z3saJkh1fQCBvCumu5QBuiLsAr6eWelxGw",
				},
			};
			chai
				.request("http://localhost:50000")
				.post(options.url)
				.set(options.headers)
				.send(options.payload)
				.end((err, res) => {
					
					_id = JSON.parse(res.text).result._id;
					console.log(err);
					res.should.have.status(400);
					done();
				});
		});
	});
	describe("PUT /update-note/{id}", () => {
		it("PUT /update-note/{id}", done => {
			let options = {
				method: "PUT",
                url: `/update-note/{id}`.replace(`{ id }`.replace(/ /g, ""), `${_id}`),
                payload:{
					title: "title",
					description: "description",
					status: "TODO",
					user: "Mayur Deshpande",
					due_date: "2021-07-04"
                    },
				headers: {
					authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJwYXNzd29yZCJ9.LBRukwCS5z3saJkh1fQCBvCumu5QBuiLsAr6eWelxGw",
				},
			};
			chai
				.request("http://localhost:50000")
				.put(options.url)
				.set(options.headers)
				.send(options.payload)
				.end((err, res) => {
					res.should.have.status(400);
					done();
				});
		});
	});
	describe("GET /get-note/{id}", () => {
		it("GET /get-note/{id}", done => {
			let options = {
				method: "GET",
				url: `/get-note/{id}`.replace(`{ id }`.replace(/ /g, ""), `${null}`),
				headers: {
					authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJwYXNzd29yZCJ9.LBRukwCS5z3saJkh1fQCBvCumu5QBuiLsAr6eWelxGw",
				},
			};
			chai
				.request("http://localhost:50000")
				.get(options.url)
				.set(options.headers)
				.end((err, res) => {
					res.should.have.status(400);
					done();
				});
		});
	});
	describe("GET /get-notes", () => {
		it("GET /get-notes", done => {
			let options = {
				method: "GET",
                url: `/get-notes`,
				headers: {
					//authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJwYXNzd29yZCJ9.LBRukwCS5z3saJkh1fQCBvCumu5QBuiLsAr6eWelxGw",
				},
			};
			chai
				.request("http://localhost:50000")
				.get(options.url)
				.set(options.headers)
				.end((err, res) => {
					res.should.have.status(401);
					done();
				});
		});
	});

	describe("DELETE /delete-note/{id}", () => {
		it("DELETE /delete-note/{id}", done => {
			let options = {
				method: "DELETE",
				url: `/delete-note/{id}`.replace(`{ id }`.replace(/ /g, ""), `${null}`),
				headers: {
					authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJwYXNzd29yZCJ9.LBRukwCS5z3saJkh1fQCBvCumu5QBuiLsAr6eWelxGw",
				},
			};
			chai
				.request("http://localhost:50000")
				.delete(options.url)
				.set(options.headers)
				.end((err, res) => {
					res.should.have.status(400);
					done();
				});
		});
	});



	describe("GET /get-csv-report", () => {
		it("GET /get-csv-report", done => {
			let options = {
				method: "GET",
                url: `/get-csv-report`,
				headers: {
					//authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJwYXNzd29yZCJ9.LBRukwCS5z3saJkh1fQCBvCumu5QBuiLsAr6eWelxGw",
				},
			};
			chai
				.request("http://localhost:50000")
				.get(options.url)
				.set(options.headers)
				.end((err, res) => {
					res.should.have.status(401);
					done();
				});
		});
	});

	after(async () => {
		// Before block
		try {
			console.log("In after..");
			let mongo_close = await mongo.close();
			result.stop();
			process.exit(0);
		} catch (e) {
			console.log("Error in starting server : ", e);
			return e;
		}
	});
});

/******************************* END TEST FUNCTIONS *******************************/

